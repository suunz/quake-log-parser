<?php

/**
* @author Érick Célio
*
*/

class homeController extends controller{
	//função que será acessada ao acessar a página home ou index.
	public function index() {
		
		//chamo a view home
		$this->loadView('home');
	}
	
}