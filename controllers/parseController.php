<?php 
/**
* @author Érick Célio
*
*/

class parseController extends controller{
	
	//função que será acessada ao acessar tentar acessar o parser.
	public function index() {

		//Salva o arquivo log la pasta do projeto.
		$arquivo = $_FILES['arquivo'];
		if(isset($arquivo['tmp_name']) && !empty($arquivo['tmp_name'])){
			$nomedoarquivo = md5(time().rand(0,99));
			move_uploaded_file($arquivo['tmp_name'], 'assets/logs/'.$nomedoarquivo);

			//intancio a classe Parser
			$arq = new Parser('assets/logs/'.$nomedoarquivo);
			$arq = $arq->dados;

			//chamo a view parse passando o array com os dados.
			$this->loadView('parse', $arq);
		}
	}
}
?>
