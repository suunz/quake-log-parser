<?php
/**
* @author Érick Célio
*
* Aqui crio duas classes a Parser, GameClass e suas funções.
*
* Parser armazenará algumas informações sobre o log e fará a leitura do mesmo.
*
* GameClass armazenará informações sobre o game..
*/
class Parser{

	private $nomeArq;
	private $qntGame = 0;
	private $game;
	private $kills;
	private $mean;
	public $dados = array();

	/*
	* função que será executa logo quando a classe for estanciada
	* armazenará o nome do arquivo e chamará a função iniciar().
	*/
	public function __construct($nomeArq){
		$this->nomeArq = $nomeArq;
		$this->iniciar();
	}

	/*
	* função que fara a abertura do arquivo em modo leitura
	* e chamara a função lerArq($log) e após a leitura será fechado.
	*/
	private function iniciar(){
		$log = fopen($this->nomeArq, 'r');

		$this->lerArq($log);

		fclose($log);
	}

	/*
	* função que fara a leitura do arquivo chamando a função getLinha($log)
	* e após ler a linha irá verificar em qual case ela irá entrar
	*/
	private function lerArq($log){
		while(!feof($log)){
			$linha = $this->getLinha($log);

			switch ($linha['command']) {
                case 'InitGame':
                    $this->initGame($linha);
                    break;
                case 'ClientUserinfoChanged':
                    $this->clientUserinfoChanged($linha);
                    break;
                case 'Kill':
                    $this->kill($linha);
                    break;
                case '------------------------------------------------------------':
                case 'ShutdownGame':
                    $this->shutdownGame($linha);
                    break;
                default:
                    break;
            }
		}
	}

	/*
	* função que fara a leitura da linha usando a quebra de linhas
	* e armazenará em um array.
	*/
	private function getLinha($file) {
        $linha = fgets($file, 4096);
        if (!empty($linha)) {
            $params = explode(":", trim($linha), 3);
            $time = explode(" ", $params[0], 2);
            $time = isset($time[1]) ? $time[1] : $time[0];
            $time = trim($time . ":" . $params[1]);
            $time_command = explode(" ", $time, 2);

            $result['params'] = isset($params[2]) ? $params[2] : '';
            $result['time'] = $time_command[0];
            $result['command'] = $time_command[1];

            return $result;
        }
        return false;
    }

    /*
	* função que será iniciada ao entrar no case InitGame, irá instanciar 
	* a classe GameClass(), definir mean como array, kills como array,
	* adicionar 1 a contagem do qntGame, e chamar a função setId baseada
	* na qntGame. 
	*/
    private function initGame($linha){
    	$this->game = new GameClass();
    	$this->mean = array();
    	$this->kills = array();
    	$this->qntGame ++;
    	$this->game->setId($this->qntGame);
    }

    /*
	* função que será iniciada ao entrar no case ClientUserinfoChanged, irá
	* verificar o nome do player e adicionar ao game ao acionar a funcão setPlayers.
	*/
    private function clientUserinfoChanged($linha){
    	$player = explode('\t\\', $linha['params'], 2);
        $player = explode(' n\\', $player[0], 2);

        if (!in_array($player[1], $this->game->getPlayers())) {
            $this->game->setPlayers($player[1]);
        }
    }

    /*
    * função que será iniciada ao entrar no case Kill, irá pegar quem matou,
    * quem morreu e a causa e chamará a função setKill.
    */
    private function kill($linha){
    	$this->game->addKill();

    	$valor = explode(":", $linha['params'], 2);
    	$valor = explode("killed", $valor[1]);
    	$p_killer = trim($valor[0]);
    	$valor = explode(" by ", trim($valor[1]));
        $p_killed = trim($valor[0]);
        $mod = trim($valor[1]);

        $this->setKill($p_killer, $p_killed);
        $this->mean[$mod] = (isset($this->mean[$mod]) ? $this->mean[$mod] : 0) + 1;
    }

    /*
    * função que fara a contagem de kills, se o player foi morto pelo <word>
    * será contabilizado -1, se ele se matou -1, e adicionar +1 caso quem matou foi um player.
    */
    private function setKill($p_killer, $p_killed) {
        if ($p_killer == "<world>") {
            $this->kills[$p_killed] = (isset($this->kills[$p_killed]) ? $this->kills[$p_killed] : 0) - 1;
        } else if ($p_killer == $p_killed) {
            $this->kills[$p_killer] = (isset($this->kills[$p_killer]) ? $this->kills[$p_killer] : 0) - 1;
        } else {
            $this->kills[$p_killer] = (isset($this->kills[$p_killer]) ? $this->kills[$p_killer] : 0) + 1;
        }
    }

    /*
    * função que será iniciada ao entrar no case ShutdownGame, irá chamar as 
    * funções para armazenar os valores com as funções setKills e setMeans,
    * e tranformar o game atual em um array.
    */
    private function shutdownGame($linha){
    	if (isset($this->game)) {
            $this->game->setKills($this->kills);
            $this->game->setMeans($this->mean);
            $this->dados[] = $this->game->toArray();
            unset($this->game);
        }
    }

    public function getDados() {
        return $this->dados;
    }


}

class GameClass {
	private $id;
	private $players = array();
	private $kill = array();
	private $means = array();
	private $total_kills;


	public function getId(){
		return $this->id;
	}


	public function getPlayers(){
		return $this->players;
	}


	public function getKills(){
		return $this->kill;
	}

	public function getMeans(){
		return $this->means;
	}

	public function getTotal_Kills(){
		return is_null($this->total_kills) ? 0 : $this->total_kills;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function setPlayers($players){
		$this->players[] = $players; 
	}

	public function setKills($kills){
		$this->kill = $kills;
	}

	public function setMeans($means){
		$this->means = $means;
	}

	public function setTotalKills($total_kills){
		$this->total_kills = $total_kills;
	}

	public function addKill(){
		$this->setTotalKills($this->getTotal_Kills() + 1);
	}

    //função que irá retornar os dados em uma array.
	public function toArray() {
        $data = array(
            'total_kills' => $this->getTotal_Kills(),
            'players' => $this->getPlayers(),
            'kills' => $this->getKills(),
            'means' => $this->getMeans()
        );
        $game = array('Game' . $this->getId() => $data);
        return $game;
    }
}