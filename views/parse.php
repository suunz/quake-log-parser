<!DOCTYPE html>
<html>
<head>
	<title>Parsing</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
    <link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css"  media="screen,projection"/>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body class="bg-secondary">

	<div class="container pt-4 pb-4">

		<a href="home" class="btn btn-primary" action="index">Enviar um novo Log</a>

	</div>

<?php 
$c = 0;
$cont = 1;
$al = count($viewData);
$nome = "Game";
	while ($c < $al) {
		$v = $viewData; 
		$v = extract ($v[$c], EXTR_PREFIX_SAME, "wddx");
		$v = extract (${$nome.$cont}, EXTR_PREFIX_SAME, "wddx");
		?>


<div class="container bg-white">

	<h3 class="d-flex justify-content-center pt-2 pb-2">Game <?php echo $cont; ?></h3>

	<div class="row ">
		<div class="col">
			<table class="table table-bordered table-striped ">
  				<thead>
    				<tr>
      					<th scope="col">Players</th>
    				</tr>
  				</thead>
  				<tbody> 
  				<?php 
  					foreach ($players as $value) {
					?>
						<tr>
							<td scope="row">
								<?php echo $value;?>	
							</td>
						</tr>
					<?php
							}
  				?>  
  				</tbody>
			</table>
		</div>
		<div class="col">
			<table class="table table-bordered table-striped ">
  				<thead>
    				<tr>
      					<th scope="col">Players</th>
      					<th scope="col">Kills</th>
    				</tr>
  				</thead>
  				<tbody> 
  				<?php 
  					foreach ($kills as $key => $value) {
  								?><tr>
								<td><?php echo ($key);?></td>
								<td><?php echo ($value);?></td>
								</tr><?php
  						}		
  				?>  
  				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<table class="table table-bordered table-striped ">
  				<thead>
    				<tr>
      					<th scope="col">Motivo da Morte</th>
      					<th scope="col">Qnt</th>
    				</tr>
  				</thead>
  				<tbody> 
  				<?php 
  					foreach ($means as $key => $value) {
							?><tr>
								<td><?php echo ($key);?></td>
								<td><?php echo ($value);?></td>
								</tr><?php
							}
  				?>  
  				</tbody>
			</table>
		</div>
		<div class="col">
			<table class="table table-dark d-flex justify-content-center">
  				<thead>
    				<tr>
      					<th scope="col">Kills Totais: <?php echo $total_kills?></th>
    				</tr>
  				</thead>
			</table>
		</div>
	</div>
	<hr>
</div>

<?php 
	unset($kills);
	unset($players);
	unset($total_kills);
	unset($means);
	unset($v);
	$cont++;
	$c++;
	} ?>

	  <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>