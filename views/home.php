<!DOCTYPE html>
  <html>
    <head>
    	
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
      <link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css"  media="screen,projection"/>
      <link rel="stylesheet" type="text/css" href="assets/css/style.css">

      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>

    <body class="bg-dark">

		<div class="container">

			<div class="row">

				<div class="col-sm-12">

					<img class="rounded mx-auto d-block"src="assets/img/icon-quake3.png">

				</div>

			</div>
			<div class="row">

				<div class="col-sm-4"></div>

				<div class="col-sm-4">

					<form method="POST" action="parse" enctype="multipart/form-data">

					<div class="form-group">

						<div class="input-group input-file" name="arquivo">

    						<input type="text" class="form-control" placeholder='Escolha um Log...' />	

            				<span class="input-group-btn">

        						<button class="btn btn-default btn-choose" type="button">Escolher</button>

    						</span>

    					</div>

					</div>

						<div class="form-group  ">

							<button type="submit" class="btn btn-primary ">Enviar</button>

						</div>
						
						<a class="btn btn-warning mt-3" href="assets/games.log" download>Clique aqui para fazer o download do Log</a>

					</form>

				</div>

				<div class="col-sm-4"></div>

			</div>
			
		</div>
		
		
      <script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
      <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="assets/js/script.js"></script>
    </body>
  </html>